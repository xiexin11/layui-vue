::: demo

<template>
    <ul class="layui-row layui-col-space15">
        <li class="layui-col-sm3">
            <div style="background-color: #009688;padding:10px;color:whitesmoke;padding:30px">
                <p>#009688</p><p>
                </p><p tips="">主色调之一</p>
            </div>
        </li>
        <li class="layui-col-sm3">
            <div style="background-color: #5FB878;padding:10px;color:whitesmoke;padding:30px">
                <p>#5FB878</p><p>
                </p><p tips="">一般用于选中状态</p>
            </div>
        </li>
        <li class="layui-col-sm3">
            <div style="background-color: #393D49;padding:10px;color:whitesmoke;padding:30px">
                <p>#393D49</p><p>
                </p><p tips="">通常用于导航</p>
            </div>
        </li>
        <li class="layui-col-sm3">
            <div style="background-color: #1E9FFF;padding:10px;color:whitesmoke;padding:30px">
                <p>#1E9FFF</p><p>
                </p><p tips="">经典蓝</p>
            </div>
        </li>
    </ul>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>
:::

::: demo

<template>
<ul class="layui-row layui-col-space15">
      <li class="layui-col-sm3">
        <div style="background-color: #FFB800;padding:10px;color:whitesmoke;padding:30px">
          <p>#FFB800</p><p>
          </p><p tips="">暖色系</p>
        </div>
      </li>
      <li class="layui-col-sm3">
        <div style="background-color: #FF5722;padding:10px;color:whitesmoke;padding:30px">
          <p>#FF5722</p><p>
          </p><p tips="">比较引人注意的颜色</p>
        </div>
      </li>
      <li class="layui-col-sm3">
        <div style="background-color: #01AAED;padding:10px;color:whitesmoke;padding:30px">
          <p>#01AAED</p><p>
          </p><p tips="">文本链接着色</p>
        </div>
      </li>
      <li class="layui-col-sm3">
        <div style="background-color: #2F4056;padding:10px;color:whitesmoke;padding:30px">
          <p>#2F4056</p><p>
          </p><p tips="">侧边色</p>
        </div>
      </li>
    </ul>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>
:::

::: demo

<template>
<ul class="layui-row site-doc-color site-doc-necolor">
      <li class="layui-col-md6">
        <div style="background-color: #FAFAFA;">
          <p>#FAFAFA</p><p>
        </p></div>
      </li>
      <li class="layui-col-md6">
        <div style="background-color: #f6f6f6;"><p>#F6F6F6</p><p></p></div>
      </li>
      <li class="layui-col-md2">
        <div style="background-color: #eeeeee;"><p>#eeeeee</p><p></p></div>
      </li>
      <li class="layui-col-md2">
        <div style="background-color: #e2e2e2;"><p>#e2e2e2</p><p></p></div>
      </li>
      <li class="layui-col-md2">
        <div style="background-color: #dddddd;"><p>#dddddd</p><p></p></div>
      </li>
      <li class="layui-col-md2">
        <div style="background-color: #d2d2d2;"><p>#d2d2d2</p><p></p></div>
      </li>
      <li class="layui-col-md2">
        <div style="background-color: #cccccc;"><p>#cccccc</p><p></p></div>
      </li>
      <li class="layui-col-md2">
        <div style="background-color: #c2c2c2;"><p>#c2c2c2</p><p></p></div>
      </li>
    </ul>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>
:::