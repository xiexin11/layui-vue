export interface SelectItem {
    label?: String,
    value?: String
}